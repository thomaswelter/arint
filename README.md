# Arint
Arint is a web framework for the browser consisting of two parts:
- state managment
- renderer

You can use both of the seperately, but it is easy to put them together.
```
<!doctype html>
<div data-text="counter"></div>
```
```javascript
import {start, render, set} from './arint.js'

const action = start({
    state: {counter: 0},
    add: x => set('counter', y => x + y)
}, render('body'))

setInterval(() => action.add(1), 1000)
```

## State managment
the `start` function is used like this:
```javscript
import {start} from './arint.js'

const action = start({
    state: {counter: 0},
    add: x => state => ({...state, counter: counter + x})
})
```

The `state` object is the initial state of the application. This also defines the types that are gonna be used. `counter` is initialized as a number with value 0, if you later try to change it to something other then a number it will fail.

`Add` is an action that can be called with `action.add(24)`, this will add 24 to `counter`.


## Actions helpers
This way of defining an action is a bit verbose so arint also exposes some helper functions. With these helpers you can define actions like this: `add: set('counter', x => x + 1)` which looks much cleaner.

Change the import to `import {get, set, pipe, append, remove, insert} from './arint.js'`. All these functions are curried.

### `get(path, data)`
- `get('some.path', {some: {path: 3}})` will return `3`
- `get(['some', 'path'], {some: {path: 4}})` will return `4`

### `set(path, value | function, data)`
- `set('some.path', 6, {some: {path: 5}})` will return `{some: {path: 6}}`
- `set('some.path', x => x * 10, {some: {path: 5}})` will return `{some: {path: 50}}`

### `pipe(...functions)`
composes functions together. the first arguments plugs into the second
example: `pipe(set('first', 3), set('second', 4))`

### `append(path, value, data)`
The value at path should be an array, this array is appended with `value`

### `remove(pathArray, data)`
If value at path is an object remove key if an array splice at index
example: `remove(['items', 3])` splices index 3

### `insert(pathArray, value, data)`
insert value at index from pathArray
example: `insert(['items', 1], 4, [3, 5])` will return `[3, 4, 5]`


## State helpers
Because state types are fixed you need to define upfront which types to expect.
add to import `import {list, dict} from './arint.js'`

### `list(array, empty)`
`state: {items: [ {name: 'the name'} ]}` defines a list of items where each item has to have a name property, other properties are denied. When doing this the state starts out with one item, if you want to start out with an empty list you can use the `list` helper.

example: `list([{name: 'a name'}], true)` initializes an empty list with types defined

### `dict(dictonary, empty)`
By default `{}` is seen by arint as a struct. This means that properties can not be changed or removed. If you want to build a hashmap you can use the `dict` helper.

example: `dict({key1: 123})` initializes a dictonary with `key1: 123` as an initial value


## Nested actions and private properties
Sometimes you want to limit the scope of an action because it has a specific purpose. Here is an example:
```javscript
import {start, set} from './arint.js'

const action = start({
    state: {
        counter: {_value: 0}
    },

    counter: {
        add: x => set('_value', y => x + y)
    }
})

action.counter.add(3) //--> 3
action.counter.add(4) //--> 7
```
Note `_value` is prefixed with an underscore, this makes `_value` only accesble inside `counter`, parents can't get or set it. You can also see that nested actions can't see their parent. Not using the prefix allows the parent to get and set the property.


## Getters
`import {getter} from './arint.js'`

Often you've got values that are derived from state but you don't want to recompute them everytime. They are part of your data structure and ideally you would want to define their dependencies and have them auto update. This is what the `getter` helper does.
```javscript
state: {
    age: 17,
    allowAlcohol: getter(state => state.age >= 18)
}
```
Each time `age` changes `allowAlcohol` is also updated. Note that the update happens internally, the state object you receive always has a boolean value at `allowAlcohol` (no getters are present). Also note that the update happens when another propery changes, this makes accessing getters cheap and setting properties (a little) more expensive.


## Renderer
How your state renders can be completely defined inside you html. Arint does this using `data-` attributes with a function behind it. to use it first import it:
`import { render } from './arint.js'`.

Normally you would use it together with the state manager, in that case you would call it like this: `start({...}, render('body'))` and Arint would take care of the rest.

If you want to use the renderer on it's own use it like this: `render(selector, action, state)` where `action('action.path.name', ...args)`

### `data-text="some.path"`
update this elements `textContent` when `state.some.path` changes

### `data-html="some.path"`
update this elements `innerHTML` when `state.some.path` changes

### `data-repeat="items as item"`
uses the contents of this element as a template which is duplicated for each item in `items`. You can uses `item` as path on all children.

### `data-style="transform: translate({x}px, {y}px);"`
update the style property when `x` or `y` changes

### `data-attr='checked="{ isChecked }"'`
updates checked attribute when `isChecked` changes

### `data-object="some.path"`
updates the elements style with a style object

### `data-if="some.value > 50"`
hide the element when the expression is false

### `data-on="click.passive: nested.action(some.path, some.path2);"`
attach listener to element. In this case a click event with a passive modifier. When this element is clicked `action('nested.action', event, 'some.path', 'some.path2')` is run. If your using the state manager Arint handles this for you.


# Todo example
```
<!doctype html>

<ul data-repeat="todos as item">
	<li>
		<h2 data-text="item.name" data-on="click: toggle(item)"></h2>
		<input type="checkbox" data-attr="checked='{item.checked}'" />
		<button data-on="click: remove(item)">X</button>
	</li>
</ul>

<input data-on="keyup: todoInput()" />

<script src="index.js" type="module"></script>
```
```javascript
import {start, set, append, remove, insert, list, getter, render} from './arint.js'

const action = start({
	state: {
		todos: list([
			{
				name: 'Buy climbing gear',
				done: false,
				checked: getter(state => state.done? 'checked' : '')
			}
		], true)
	},

	toggle: (ev, path) =>
		set([...path, 'done'], x => !x),

	remove: (ev, path) =>
		remove(path),

	todoInput: ({key, target}) => {
		if(key == 'Enter')
			return append('todos', {name: target.value})
	}
}, render('body'))
```