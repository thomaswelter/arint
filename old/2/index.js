import {arint, h} from './src/arint.js'

const app = new Arint

app.action('add', state => ({...state, count: state.count + 1}))

const counter = app.template('count', count => h('div', [
	h('span', [count]),
	h('button', {
		onclick: ev => app.emit('add')
	}, ['Add'])
]))

const root = app.template(() => h('div', [
	h('h1', ['some text']),
	h('p', ['some paragraph']),
	counter
]))

app.render(root, document.body)