export const renderTemplate = (f, state) => {
	// get old vnodes
	const a = templateVnodes.get(f)

	// get deps from template, conv to path, conv to partial, run template with partials
	const b = f(...templateDeps.get(f).map(dotNotationToPath).map(toPartial(state)))

	// create Actual/update dom elements
	const element = patchDom(a, b, templateDomElements.get(f))

	// update template refs to element and vnode
	templateDomElements.set(f, element)
	templateVnodes.set(f, b)

	return element
}

// a and b are vnodes or strings or Symbols (template), element is a dom element
export const patchDom = (a, b, element) => {
	if(typeof a == 'symbol') {
		a = templateVnodes.get(templateSymbols.get(a))
		element = templateDomElements.get(a)
	}

	if(typeof b == 'symbol') {
		const f = templateSymbols.get(b)
		b = templateVnodes.get(f)
		if(!b) {
			renderTemplate(f, globalState)
			b = templateVnodes.get(f)
		}
	}

	let k

	if(a === b)
		return element

	else if(a && !b)
		return element.remove()

	else if(!a && !b)
		return

	else if(typeof a == 'string' && typeof b == 'string') {
		element.nodeValue = b
		return element
	}

	else if((!a && b) || a.tagName !== b.tagName) {
		element && element.remove()

		if(typeof b == 'string')
			return document.createTextNode(b)

		element = document.createElement(b.tagName)

		for(k in b.props)
			setAttr(element, k, b.props[k])

		for(k in b.children)
			element.appendChild(patchDom(undefined, b.children[k], undefined))

		return element
	}

	else if(a && b) {
		for(k in b.props)
			a.props[k] !== b.props[k] && setAttr(element, k, b.props[k], a.props[k])

		for(k in a.props)
			b.props[k] === undefined && removeAttr(element, k, a.props[k])

		for(k in b.children)
			patchDom(a.children[k], b.children[k], element.childNodes[k])

		return element
	}
}

export const deepFreeze = obj => {
	for(let key of Object.getOwnPropertyNames(obj)) {
		obj[key] = obj[key] && typeof obj[key] == 'object'
			? deepFreeze(obj[key])
			: obj[key]
	}

	return Object.freeze(obj)
}

const isPrimitive = val =>
	['boolean', 'number', 'string', 'undefined'].indexOf(typeof val) > -1 || val === null

export const diff = (a, b, path = [], detail = false) => {
	if(a === b)
		return []

	if(!isPrimitive(b)) {
		return Object.keys(b).concat(isPrimitive(a)? []: Object.keys(a))
			// filter double common keys
			.filter((n, i, r) => r.indexOf(n) == i)

			// recursive diff
			// detail: also log changed objects {} -> {}
			.reduce((acc, k) => acc.concat(diff((isPrimitive(a)? undefined : a[k]), b[k], path.concat(k)))
				, [(detail? [a, b, path] : [])])

			// filter empty logs
			.filter(x => x.length)

			// filter removal of keys
			// .filter(x => x[1] !== undefined)
	}

	return [[a, b, path]]
}

export const dotNotationToPath = str =>
	str.split(/\.|\[|\]/).filter(x => !!x)

export const pathIsEqual = (a, b) =>
	a.findIndex((x, i) => x !== b[i]) == -1

export const toPartial = state => path =>
	path.reduce((a, c) => a[c], state)

export const convertToVnode = val =>
	isPrimitive(val)? val + '' : val

export const setAttr = (element, k, b, a) => k.slice(0, 2) == 'on'
	? (element.removeEventListener(k.slice(2), a), element.addEventListener(k.slice(2), b))
	: element.setAttribute(k, b)

export const removeAttr = (element, k, v) => k.slice(0, 2) == 'on'
	? element.removeEventListener(k.slice(2), v)
	: element.removeAttribute(k)

export const log = val =>
	(console.log(val), val)