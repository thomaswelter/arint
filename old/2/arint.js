import {
	deepFreeze,
	diff
} from './functions.js'

export const h = (tagName, ...args) => args.length == 1
	? {tagName, props: {}, children: args[0].map(convertToVnode)}
	: {tagName, props: args[0], children: args[1].map(convertToVnode)}

const templates = new Map
const shouldTemplateUpdate = new Set
const actions = new Map
let globalState

export class Arint {
	action(name, f) {
		actions.set(name, f)
	}

	template(...args) {
		const tf = args.slice(-1)[0]
		const needs = args.slice(0, -1)
		const f = () => {}

		templates.set(f, {tf, needs, children: []})
		needs.forEach(need => templates.set(need, (templates.get(need) || []).concat(f)))
		return f
	}

	emit(name, ...data) {
		const f = actions.get(name)
		const oldState = globalState
		const newState = deepFreeze(f(globalState, ...))
		const templates = diff(oldState, newState)

			// get templates that have diff as an dependency
			.reduce((l, [a, b, path]) => l.concat(templates.get(path.join('.'))), [])

			// paths that don't have any templates should be filtered
			.filter(f => !!f)

			// filter double templates
			.filter((f, i, r) => r.indexOf(f) == i)

		shouldTemplateUpdate.add(templates)
	}

	render(tid, element) {
		//
	}
}