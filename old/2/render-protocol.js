
const isPrimitive = val =>
	['boolean', 'number', 'string', 'undefined'].indexOf(typeof val) > -1 || val === null

const convertToVnode = val =>
	isPrimitive(val)? val + '' : val

const h = (tagName, ...args) => args.length == 1
	? {tagName, props: {}, children: args[0].map(convertToVnode)}
	: {tagName, props: args[0], children: args[1].map(convertToVnode)}

const createWorker = f =>
	new Worker(URL.createObjectURL(new Blob(['self.onmessage='+f], {type: 'text/javascript'})))

const worker = createWorker(ev => {

	const patchvdom = (a, b, path = []) => {
		// if(typeof a == 'symbol') {
		// 	a = templateVnodes.get(templateSymbols.get(a))
		// 	element = templateDomElements.get(a)
		// }

		// if(typeof b == 'symbol') {
		// 	const f = templateSymbols.get(b)
		// 	b = templateVnodes.get(f)
		// 	if(!b) {
		// 		renderTemplate(f, globalState)
		// 		b = templateVnodes.get(f)
		// 	}
		// }

		let k, actions = []

		if(a === b)
			return []

		else if(a && !b)
			return [['removeNode', path]]

		else if(!a && !b)
			return []

		else if(typeof a == 'string' && typeof b == 'string')
			return [['updateTextNode', path, b]]

		else if(!a && b) {
			if(typeof b == 'string')
				return [['createTextNode', path, b]]

			actions.push(['createNode', path, b.tagName])

			for(k in b.props)
				actions.push(['setAttribute', path, +k, b.props[k]])

			for(k in b.children)
				actions.push(...patchvdom(undefined, b.children[k], path.concat(+k)))

			return actions
		}

		else if(a && b) {
			if(a.tagName !== b.tagName)
				actions.push(['replaceNode', path, b.tagName])

			if(typeof a == 'string' && b.tagName) {
				for(k in b.props)
					actions.push(['setAttribute', path, +k, b.props[k]])

				for(k in b.children)
					actions.push(...patchvdom(undefined, b.children[k], path.concat(+k)))

				return actions
			}

			else if(b.tagName && typeof a == 'string')
				return [['removeNode', path], ['createTextNode', path, a]]

			for(k in b.props)
				(a.props[k] !== b.props[k]) && actions.push(['setAttribute', path, +k, b.props[k]])

			for(k in a.props)
				b.props[k] === undefined && actions.push(['removeAttribute', +k, a.props[k]])

			for(k in b.children)
				actions.push(...patchvdom(a.children[k], b.children[k], path.concat(+k)))

			for(k in a.children)
				b.children[k] === undefined && actions.push(...patchvdom(a.children[k], undefined, path.concat(+k)))

			return actions
		}
	}

	const data = patchvdom(self.vdom, ev.data)
	self.vdom = ev.data
	self.postMessage(data)
})

let parent = document.body
let element
const actions = []

worker.onmessage = ev => {
	const len = actions.length
	actions.push(...ev.data)

	if(!len)
		requestAnimationFrame(step)
}

const step = t => {
	while(actions.length) {
		const [action, path, ...args] = actions.shift()
		let el = element, p = parent

		console.log(p, el, path, action, ...args);

		while(path.length) {
			p = el
			el = el.childNodes[path.shift()]
		}

		console.log(p, el);

		if(action == 'replaceNode') {
			const l = document.createElement(args[0])
			element = p.appendChild(l)

			while(el.childNodes.length)
				l.appendChild(el.childNodes[0])

			el.remove()
		}

		else if(action == 'createNode') {
			const l = document.createElement(args[0])
			if(!element) element = p.appendChild(l)
			else p.appendChild(l)
		}

		else if(action == 'createTextNode')
			p.appendChild(document.createTextNode(args[0]))

		else if(action == 'updateTextNode')
			el.nodeValue = args[0]

		else if(action == 'removeNode')
			el.remove()

		else if(action == 'setAttribute')
			el.setAttribute(args[0], args[1])

		else if(action == 'removeAttribute')
			el.removeAttribute(args[0])

		else if(action == 'addEventListener')
			el.addEventListener(args[0], args[1])

		else if(action == 'removeEventListener')
			el.removeEventListener(args[0], args[1])

		if(performance.now() - t > 10)
			return requestAnimationFrame(step)
	}
}

worker.postMessage(h('div', ['some test']))
