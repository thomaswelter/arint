# definitions
tid (template id) -> f \ symbol
need -> partial path string 'path.panel.count'
path -> array of state props. ['path', 'panel', 'count']
partial -> part of a state, {count: 0} where need = 'path.panel'
vnode -> {tagName: 'div', props: {}, children: [vnode, ...]}
vdom -> root vnode
element -> HTMLElement
name -> string name of function. 'input/x/target'
renderAction -> instruction to update DOM (see below)

# variables
templates:
	tid: {tf, needs: [need, ...], children: [tid, ...], vdom: vnode}
	need: [tid, ...]

globalState: {...}

shouldTemplateUpdate: new Set -> tid, ...

actions: name -> f

# functions
app.action(name, f):
	actions.set(name, f)

app.emit(name, ...data):
	newState = actions.get(name)(globalState, ...data)
	diff(globalState, newState) -> templates
	-> update shouldTemplateUpdate
    -> updateTemplate(tid, renderedvdom)

updateTemplate(tid, renderedvdom, ...elements)
    if shouldTemplateUpdate: templates.get(tid).vdom = templates.get(tid).tf(...)
    patchvdom(renderedvdom, templates.get(tid).vdom) >
    < -- {renderActions: [...], renderedvdom, elements, tid}

render():
    queue -> [...]

app.render(root, element):
    updateTemplate(root, undefined, undefined)

# renderAction
//    {action: 'createElement', path: [0,0,3,4], value: 'div'}
//    {action: 'createProp', path: [0,1,1], propName: 'test', value: 'abc'}

# render protocol
