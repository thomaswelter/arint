import {
	deepFreeze,
	diff,
	dotNotationToArray,
	pathIsEqual
} from './src/functions.js'

const test = (name, fn) => fn({
	true: function(val) {
		document.body.innerHTML += `<div class="${val? 'pass' : 'fail'}">${name}</div>`
	},

	false: function(val) {
		this.true(!val)
	},

	equal: function(a, b) {
		this.true(JSON.stringify(a) == JSON.stringify(b))
	}
})

test('deepfreeze objects stay frozen', t => {
	const obj = {a: {b: 3}}
	const tobj = deepFreeze(obj)
	// the following throws an error
	// obj.a.b = 5
	t.true(obj.a.b == tobj.a.b)
})

test('diff returns diff', t => {
	const a = {a: {b: 3}, c: 'test'}
	const b = {...a, c: 'diff'}
	const d = diff(a, b)
	t.equal(d, [
		[a, b, []],
		['test', 'diff', ['c']]
	])
})

test('dotNotationToArray', t => {
	const arr = dotNotationToArray('one.two[three].four')
	t.equal(arr, ['one', 'two', 'three', 'four'])
})

test('pathIsEqual', t => {
	t.true(pathIsEqual(['a', 'b'], ['a', 'b']))
})

test('pathIsEqual false', t => {
	t.false(pathIsEqual(['a', 'c'], ['a', 'b']))
})