import Arint, {Action} from './Arint.js'

Arint(
	{
		todos: [],
		filter: 'all',
		filteredTodos: [],
	},

	state => (
		<link rel="stylesheet" href="style.css"/>
		<ul>
			<li repeat={state.todos} class={state.todos.n.done && 'done'}>
				<h3>{state.todos.n.text}</h3>
				<a href={Action('check todo', state.todo.n.key)}>check</a>
			</li>
		</ul>
	),

	{
		'check todo': (state, item) => {
			item.done = true
		}
	}
)