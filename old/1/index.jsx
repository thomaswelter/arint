import Arint from './arint.js'

const Foo = state => (
	<div>some app</div>
)

Arint(
	{
		test: '123',
		arr: [1,2,3,{a: 1}]
	},

	state => (
		<div>
			<h1>Test: {state.test}</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius repudiandae cum voluptatem consequuntur aliquid nesciunt quidem voluptates ipsum illum, modi adipisci voluptas expedita corporis, aliquam, perspiciatis et quis, recusandae vel.</p>
			<Foo />
		</div>
	),

	{
		'action': (state) => {
			// do something
		}
	}
)