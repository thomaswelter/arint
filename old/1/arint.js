let State = {}
let Template = null
let Reducer = null
let Elements = []
let key = 0

const generateKey = () => key++
const isBrowser = (typeof window == 'object')
const isArray = x => Array.isArray(x)
const isPrimitive = x => (['string', 'number', 'boolean'].indexOf(typeof x) > -1)
const isObject = x => (typeof x == 'object' && !isArray(x) && !isPrimitive(x))
const kv2obj = x => x.reduce((a, [k, v]) => ({...a, [k]: v}), {})
const mapValues = fn => obj => Object.entries(obj).map(([k, v]) => [k, fn(v)])
const fold = (...x) => x.reduce((v, fn) => fn(v))
const applyKeys = () => {
	Elements.forEach(item => {
		if(item.isCustom)
			return

		item.element = document.querySelector(`[arint-key="${item.key}"]`)
		item.element.removeAttribute('arint-key')
	})
	key = 0	
}

const Arint = (data, template, reducer) => {
	State = data

	console.log(sealData(data));

	document.body.innerHTML = template(data).toString()
	applyKeys()

	console.log(Elements);
}


const sealData = (data) => {
	if(isPrimitive(data))
		return data

	if(isArray(data))
		return fold(sealData, x => data.map(x), Object.seal)

	if(isObject(data))
		return fold(data, mapValues(sealData), kv2obj, Object.seal)
}

const shadowObject = class {
	constructor(data) {
		this.data = data
		this.writable = true
		this.symbolMode = false
		this.proxyCommand = {
			get: (t, p, r) => {
				//
			}
		}
	}

	proxy(d) {
		return new Proxy(d, this.proxyCommand)
	}
}

const Element = class Element {
	constructor(element, props, ...children) {
		this.isCustom = (typeof element == 'function')
		this.tagName = element
		this.props = props || {}
		this.children = children
		this.key = generateKey()
		this.props['arint-key'] = this.key
		this.element = null
		Elements.push(this)

		if(this.isCustom)
			this.template = element(State)
	}

	toString() {
		if(this.isCustom)
			return this.template.toString()

		const tagName = this.tagName
		const content = this.children.join('')
		const props = Object.entries(this.props).length
			? Object
			.entries(this.props)
			.reduce((a, [k, v]) => [...a, `${k}="${v}"`], [])
			.join(' ')
			: ''

		return `<${tagName} ${props}>${content}</${tagName}>`
	}
}

Arint.createElement = (...params) => new Element(...params)

const Action = (name, ...params) => {
	//
}

export default Arint
export { Action }