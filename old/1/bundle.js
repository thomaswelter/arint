(function () {
	'use strict';

	var State = {};
	var Elements = [];
	var key = 0;

	var generateKey = function () { return key++; };
	var isArray = function (x) { return Array.isArray(x); };
	var isPrimitive = function (x) { return (['string', 'number', 'boolean'].indexOf(typeof x) > -1); };
	var isObject = function (x) { return (typeof x == 'object' && !isArray(x) && !isPrimitive(x)); };
	var kv2obj = function (x) { return x.reduce(function (a, ref) {
		var obj;

		var k = ref[0];
		var v = ref[1];
		return (Object.assign({}, a, ( obj = {}, obj[k] = v, obj )));
		}, {}); };
	var mapValues = function (fn) { return function (obj) { return Object.entries(obj).map(function (ref) {
		var k = ref[0];
		var v = ref[1];

		return [k, fn(v)];
		}); }; };
	var fold = function () {
		var x = [], len = arguments.length;
		while ( len-- ) x[ len ] = arguments[ len ];

		return x.reduce(function (v, fn) { return fn(v); });
	};
	var applyKeys = function () {
		Elements.forEach(function (item) {
			if(item.isCustom)
				{ return }

			item.element = document.querySelector(("[arint-key=\"" + (item.key) + "\"]"));
			item.element.removeAttribute('arint-key');
		});
		key = 0;	
	};

	var Arint = function (data, template, reducer) {
		State = data;

		console.log(sealData(data));

		document.body.innerHTML = template(data).toString();
		applyKeys();

		console.log(Elements);
	};


	var sealData = function (data) {
		if(isPrimitive(data))
			{ return data }

		if(isArray(data))
			{ return fold(sealData, function (x) { return data.map(x); }, Object.seal) }

		if(isObject(data))
			{ return fold(data, mapValues(sealData), kv2obj, Object.seal) }
	};

	var Element = (function () {
		function Element(element, props) {
		var children = [], len = arguments.length - 2;
		while ( len-- > 0 ) children[ len ] = arguments[ len + 2 ];

			this.isCustom = (typeof element == 'function');
			this.tagName = element;
			this.props = props || {};
			this.children = children;
			this.key = generateKey();
			this.props['arint-key'] = this.key;
			this.element = null;
			Elements.push(this);

			if(this.isCustom)
				{ this.template = element(State); }
		}

		Element.prototype.toString = function toString () {
			if(this.isCustom)
				{ return this.template.toString() }

			var tagName = this.tagName;
			var content = this.children.join('');
			var props = Object.entries(this.props).length
				? Object
				.entries(this.props)
				.reduce(function (a, ref) {
					var k = ref[0];
					var v = ref[1];

					return a.concat( [(k + "=\"" + v + "\"")]);
			}, [])
				.join(' ')
				: '';

			return ("<" + tagName + " " + props + ">" + content + "</" + tagName + ">")
		};

		return Element;
	}());

	Arint.createElement = function () {
		var params = [], len = arguments.length;
		while ( len-- ) params[ len ] = arguments[ len ];

		return new (Function.prototype.bind.apply( Element, [ null ].concat( params) ));
	};

	var Foo = function (state) { return (
		Arint.createElement( 'div', null, "some app" )
	); };

	Arint(
		{
			test: '123',
			arr: [1,2,3,{a: 1}]
		},

		function (state) { return (
			Arint.createElement( 'div', null, 
				Arint.createElement( 'h1', null, "Test: ", state.test ), 
				Arint.createElement( 'p', null, "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius repudiandae cum voluptatem consequuntur aliquid nesciunt quidem voluptates ipsum illum, modi adipisci voluptas expedita corporis, aliquam, perspiciatis et quis, recusandae vel." ), 
				Arint.createElement( Foo, null )
			)
		); },

		{
			'action': function (state) {
				// do something
			}
		}
	);

}());
