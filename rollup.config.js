import buble from 'rollup-plugin-buble'

const bubleOptions = {
	transforms: {dangerousForOf: true},
	objectAssign: 'Object.assign'
}

export default [
	{
		input: 'src/index.js',
		output: {
			file: 'dist/Arint.es.js',
			format: 'es',
			name: 'Arint'
		},
		plugins: [
			buble(bubleOptions),
		],
	},
	{
		input: 'src/index.js',
		output: {
			file: 'dist/Arint.cjs.js',
			format: 'cjs',
			name: 'Arint'
		},
		plugins: [
			buble(bubleOptions),
		],
	},
	{
		input: 'src/index.js',
		output: {
			file: 'dist/Arint.iife.js',
			format: 'iife',
			name: 'Arint'
		},
		plugins: [
			buble(bubleOptions),
		],
	}
]