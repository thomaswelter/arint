export const diffState = (a, b) => {
	if(a === b) return {}

	const res = Array.isArray(b)? [] : {}
	if(Object(a) !== a) a = {}

	for(let k in b) {
		if(a[k] === b[k]) continue
		else if(Object(b[k]) === b[k]) res[k] = diffState(a[k], b[k])
		else res[k] = b[k]
	}

	for(let k in a)
		if(!b.hasOwnProperty(k)) res[k] = undefined

	return res
}

export const iterateDiff = (diff, f, path = '') => {
	for(let k in diff) {
		if(f(path + k) == false) return false
		if(Object(diff[k]) === diff[k])
			if(iterateDiff(diff[k], f, path + k + '.') == false) return false
		delete diff[k]
	}
}

export const merge = (target, source) => {
	for(let k in source) {
		if(Object(source[k]) === source[k]) {
			if(Object(target[k]) !== target[k])
				target[k] = Array.isArray(source[k])? [] : {}

			merge(target[k], source[k])

			// recalculate array length
			if(Array.isArray(target[k])) {
				let i = target[k].length
				while(i > 0 && !target[k].hasOwnProperty(i -1)) i--
				target[k].length = i
			}
		}

		else target[k] = source[k]
	}
}

export const MultiValueHashMap = () => {
	const store = {}
	const token = '.'

	return {
		get(key) {
			const arr = []
			while(store[key]) {
				arr.push(store[key])
				key += token
			}
			return arr
		},

		add(key, value) {
			while(store[key]) key += token
			store[key] = value
		},

		remove(key, value) {
			while(store[key] && store[key] !== value) key += token
			let last = key
			while(store[last + token]) last += token
			store[key] = store[last]
			delete store[last]
		},

		removePrefixIndex(key, index) {
			const r = new RegExp('^' + key + '\\.(\\d+)')
			for(let k in store) {
				const m = k.match(r)
				if(m && +m[1] >= index) delete store[k]
			}
		},

		removeAll(key) {
			while(store[key]) {
				delete store[key]
				key += token
			}
		}
	}
}

export const decodeOnDirective = str => {
	const event = event => {
		const [name, fns] = event.split(':')
		return {...evName(name), functions: fns.split(/\),/).map(fn)}
	}

	const evName = name => {
		const [evName, ...mod] = name.split('.')
		return {evName, modifiers: mod.reduce((a, c) => ({...a, [c]: true}), {})}
	}

	const fn = f => {
		const [name, args] = f.split('(')
		return {name, args: args.split(',').filter(x => x.length)}
	}

	return str.replace(/\s+/g, '').replace(/\);?$/, '').split(/\);/).map(event)
}

export const decodeAttr = str => {
	let onKey = true, tokens = str.split('')
	let key = '', value = '', attrs = [], stopToken

	while(tokens.length) {
		const t = tokens.shift()
		if(t == '\\') tokens.shift()
		else if(onKey) {
			if(t.match(/\s/)) continue
			else if(t.match(/[\w]/)) key += t
			else if(t == '=') {
				onKey = false
			}
		}
		else if(!onKey && !stopToken) {
			if(t.match(/\s/)) continue
			stopToken = t
		}
		else {
			if(t == stopToken) {
				onKey = true
				attrs.push([key, value])
				value = ''
				key = ''
				stopToken = false
				continue
			}
			value += t
		}
	}

	return attrs
}

export const followPath = (path, xs) =>
	(typeof path == 'string'? path.split('.') : path).reduce((a, c) => a && a[c], xs)

export const replaceName = (short, long) => str =>
	str.replace(new RegExp('^' + short), long)

// buble does not convert nodeLists to proper array so use Array.slice
export const getDirectives = (el, directive) =>
	Array.prototype.slice.call(el.querySelectorAll(`[data-${directive}]`), 0)
		.map(x => [x, x.getAttribute(`data-${directive}`)])

export const getPathsInString = str => {
	let match, r = /\{(.*?)\}/g, list = []
	while((match = r.exec(str)) !== null) list.push(match[1])
	return list
}

export const replacePathsInString = (str, nameToPath, state) => {
	let match
	while((match = /\{(.*?)\}/g.exec(str)) !== null) {
		const res = followPath(nameToPath(match[1]), state)
		if(res === undefined) return ''
		str = str.replace(match[0], res + '')
	}
	return str
}

export const render = (selector, action, newState) => {
	if(!selector) return (...a) => render(...a)
	if(!action) return (...a) => render(selector, ...a)

	let state, oldState, rafId

	const elements = MultiValueHashMap()
	const diff = {}
	const domQueue = []
	const listenerElements = new WeakSet

	const raf = () => {
		while(domQueue.length) domQueue.shift()()
		rafId = null
	}

	const update = (parent, nameToPath) => {
		getDirectives(parent, 'repeat').forEach(([el, str]) => {
			const [relativePath, name] = str.split(' as ')
			const path = nameToPath(relativePath)
			const clone = el.cloneNode(true)
			const pool = document.createDocumentFragment()
			const num = el.childNodes.length
			let length = 0

			const updateRepeat = value => {
				if(length > value.length) {
					elements.removePrefixIndex(path, value.length)

					domQueue.push(() => {
						while(el.childNodes.length > num * value.length)
							pool.insertBefore(el.lastChild, pool.firstChild)
					})
				}

				else if(length < value.length) {
					const frag = document.createDocumentFragment()

					for(let i = length; i < value.length; i++) {
						const elFrag = document.createDocumentFragment()
						for(let j = 0; j < num; j++) {
							const node = pool.firstChild || clone.childNodes[j].cloneNode(true)
							elFrag.appendChild(node)
						}
						update(elFrag, replaceName(name, path + '.' + i))
						frag.appendChild(elFrag)
					}

					domQueue.push(() => el.appendChild(frag))
				}

				length = value.length
			}

			elements.add(path, updateRepeat)
			el.textContent = ''
			const currentValue = followPath(path, state)
			currentValue && updateRepeat(currentValue)
		})

		getDirectives(parent, 'html').forEach(([el, str]) => {
			const path = nameToPath(str)
			const updateHtml = value => domQueue.push(() => {
				el.innerHTML = value
				update(el, nameToPath)
			})
			elements.add(path, updateHtml)
			updateHtml(followPath(path, state))
		})

		getDirectives(parent, 'text').forEach(([el, str]) => {
			const path = nameToPath(str)
			const updateText = value => domQueue.push(() => el.textContent = value)
			elements.add(path, updateText)
			updateText(followPath(path, state))
		})

		getDirectives(parent, 'style').forEach(([el, str]) => {
			getPathsInString(str).forEach(relativePath => {
				const updateStyle = _ => {
					const value = replacePathsInString(str, nameToPath, state)
					domQueue.push(() => el.setAttribute('style', value))
				}
				elements.add(nameToPath(relativePath), updateStyle)
				updateStyle()
			})
		})

		getDirectives(parent, 'attr').forEach(([el, str]) => {
			getPathsInString(str).forEach(relativePath => {
				const updateAttr = _ => {
					const value = replacePathsInString(str, nameToPath, state)
					const attributes = decodeAttr(value)
					domQueue.push(() => {
						attributes.forEach(([k, v]) => {
							if(v) el.setAttribute(k, v)
							else el.removeAttribute(k)
						})
					})
				}
				elements.add(nameToPath(relativePath), updateAttr)
				updateAttr()
			})
		})

		getDirectives(parent, 'object').forEach(([el, str]) => {
			const path = nameToPath(str)
			const updateAttr = value => {
				const data = followPath(path, state)
				domQueue.push(() => {
					for(let k in data) {
						if(k == 'style') for(let kk in data[k]) el.style[kk] = data[k][kk]
						else el.setAttribute(k, data[k])
					}
				})
			}
			elements.add(path, updateAttr)
			updateAttr(followPath(path, state))
		})

		getDirectives(parent, 'if').forEach(([el, str]) => {
			const [relativePath, op, string] = str.split(' ')
			const path = nameToPath(relativePath)
			const updatePred = value => {
				const data = followPath(path, state)
				const bool = 
					op == undefined? !!value :
					op == '=='? value == string :
					op == '/='? value != string :
					op == '<'? value < +string :
					op == '<='? value <= +string :
					op == '>'? value > +string :
					op == '>='? value >= +string :
					false

				domQueue.push(() => el.style.display = bool? '' : 'none')
			}
			elements.add(path, updatePred)
			updatePred(followPath(path, state))
		})

		getDirectives(parent, 'on').forEach(([el, str]) => {
			if(listenerElements.has(el)) return
			listenerElements.add(el)

			const events = decodeOnDirective(str)
			events.forEach(({evName, modifiers, functions}) => {
				functions.forEach(({name, args}) => {
					const listenFn = ev =>
						action(name, ev, ...args.map(a => nameToPath(a).split('.')))
					el.addEventListener(evName, listenFn, modifiers)
				})
			})
		})
	}

	update(document.body, x => x)

	const stateUpdate = newState => {
		state = newState
		merge(diff, diffState(oldState, state))

		iterateDiff(diff, path => {
			elements.get(path).forEach(f => f(followPath(path, state)))
		})

		oldState = state
		!rafId && requestAnimationFrame(raf)
	}

	if(!newState) return stateUpdate
	else stateUpdate(newState)
}