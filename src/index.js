export {
	start, dict, list, getter,
	get, set, pipe, append, remove, insert
} from './framework.js'

export {render} from './render.js'