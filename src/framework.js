// Arint attaches these symbols to alter the way it type checks
const dictSymbol = Symbol('Dict')
const listSymbol = Symbol('List')
const getterSymbol = Symbol('Getter')

// take an array or string path and follow it on xs
const followPath = xs => path =>
	toArrPath(path).reduce(([d, a], c) => !undef(d)? [d[isF(c)? c(d, a) : c], a] : [d, a], [xs, xs])[0]

const resolvePath = (path, xs) => {
	return toArrPath(path).reduce(([a, ls, gs], c) => {
		const r = isF(c)? c(ls, gs) : c
		return [a.concat(r), ls[r], gs]
	}, [[], xs, xs])[0]
}

const isF = x =>
	typeof x === 'function'

const undef = x =>
	x === undefined

const toArrPath = x =>
	typeof x === 'string'? x.split('.').filter(x => x.length) : x.slice()

// curries supplied function: curry((a, b) => a+b)(5) -> b => 5+b
// calling with zero arguments is the same as calling with undefined
const curry = (f, ...a) =>
	f.length > a.length? (...b) => curry(f, ...a, ...(b.length? b : [undefined])) : f(...a)

// does not copy over getters
const deepClone = (o) => {
	if(Object(o) !== o) return o
	const n = Array.isArray(o)? [] : {}
	for(let k in o) n[k] = deepClone(o[k])
	return n
}

// takes a just deepCloned state and turns all getters into values
const resolveGetters = (state, ref) => {
	// resolve deeper getters first
	for(let k in state) {
		if(Object(state[k]) === state[k]) {
			const refk = Array.isArray(ref)? ref[listSymbol] || ref[0] : ref[dictSymbol] || ref[k]
			resolveGetters(state[k], refk)
		}
	}

	// update getters until they stabalize
	let change = true
	while(change) {
		change = false
		for(let k in state) {
			if(ref[k].hasOwnProperty(getterSymbol)) {
				const old = state[k]
				state[k] = ref[k](state)
				Object.defineProperty(ref[k], getterSymbol, {value: state[k]})
				if(old !== state[k]) change = true
			}
		}
	}
}

// apply x at path on xs. always returns a promise.
// does not change anything when x is the same or if x is undefined
// does not copy getters
const update = ([k, ...kx], x, xs) => {
	return Promise.resolve(kx.length? update(kx, x, xs[k]) : x).then(v => {
		// note: if v === xs it also returns xs
		if(k === undefined) return v === undefined? xs : v
		if(v === xs[k] || v === undefined) return xs
		return Object.assign(Array.isArray(xs)? [] : {}, xs, {[k]: v})
	})
}

// checks types, update getter values, copy not defined keys to new state
// old: the old state
// now: the new state
// ref: actions.state is used to check types
// actionPath: action path without action name, eg. ['ui', 'settings']
// p: the current path
// q: a list of lists about objects that changed between old and now
const afterUpdate = (old, now, ref, actionPath, actionName) => {
	if(old === now) return
	const q = [[old, now, ref, [], undefined]]
	let k, i, p = [], c = true

	// build a list of objects that changed between old and now
	// the list is build from outer to inner
	for(i = 0; i < q.length; i++) {
		[old, now, ref, p] = q[i]

		for(k in now) {
			// ref is used as a template so keys in now might not exist in ref, but they should have the same shape
			// see also dict() and list()
			const refk = 
				Array.isArray(ref)? ref[listSymbol] || ref[0] :
				ref.hasOwnProperty(dictSymbol)? ref[dictSymbol] :
				ref[k].hasOwnProperty(getterSymbol)? ref[k][getterSymbol] :
				ref[k]

			if(typeof refk !== typeof now[k])
				return new Error(`action ${actionPath.concat(actionName).join('.')}:\nat: state.${p.concat(k).join('.')} types do not match, (${typeof refk} /= ${typeof now[k]}) value is: ${JSON.stringify(now[k])}`)

			// parents should not be able to change _props on children
			if(k[0] == '_' && actionPath.join('.') !== p.join('.') && ref[k] === refk)
				return new Error('Can not set private propert from parent')

			// if different and is array or object
			if((!old || now[k] !== old[k]) && Object(now[k]) === now[k])
				q.push([old && old[k], now[k], refk, p.concat(k)])
		}
	}	

	// loop over q in reverse order because getters can depent on children but children getters cannot depend on parents
	while(q.length) {
		[old, now, ref, p] = q.pop()

		// lists or dicts cannot have getters and values shouldn ot be copied over
		if(Array.isArray(ref) || ref[dictSymbol]) continue

		// getters can depend on other getters so we keep updating until we have a run where no getter is updated
		while(c) {
			c = false
			for(k in ref) {
				// copy over values not defined in now
				if(now[k] === undefined) now[k] = old? old[k] : ref[k]

				// if there is a getter call it (on ref) and keep looping if it changes value
				if(ref[k].hasOwnProperty(getterSymbol)) {
					const v = now[k]
					now[k] = ref[k](now)
					if(now[k] !== v) c = true
				}
			}
		}
	}
}

// this is the first function that is called
// always returns an action function
export const start = (actions = {}, render) => {
	if(Object(actions.state) !== actions.state || Array.isArray(actions.state))
		throw new Error('Need actions.state to startup')

	// cloning converts getters to values
	let state = deepClone(actions.state)
	resolveGetters(state, actions.state)

	if(typeof window !== 'undefined')
		window.state = state

	// this holds the state of arint.
	// the first 3 arguments are called by start and the rest is the action function returned by start
	// do (name, arg, ...args) so curry sees 2 arguments and so only runs after a min of 2+ args is given
	// runAction always returns a Promise with an updated state or the old state (in case of an error)
	const runAction = curry((name, arg, ...args) => {
		// action: action function
		const action = followPath(actions)(name)
		if(!action) {
			// console.warn(name + ' is called but is not defined')
			return Promise.resolve(state)
		}

		const old = state
		const actionPath = name.split('.').slice(0, -1)
		const actionName = name.split('.').slice(-1)[0]
		const oldPartial = followPath(old)(actionPath)

		// can return a function or a promise that resolves to a function
		return Promise.resolve(action(arg, ...args))

		// that function should be called with the old state or a partial if it is a nested action
		// if it is something else the old state is kept
		.then(computePartial => isF(computePartial)? computePartial(oldPartial) : undefined)
		
		// that can again return a promise which should resovle to a partial (if nested) or a state
		.then(partial => update(actionPath, partial, old))

		// that is then turned into a newState object
		.then(newState => {
			// then check types, update getters and copy not defined props from old to newState
			// returns an Error on TypeError or when not allowed to set private property
			const err = afterUpdate(old, newState, actions.state, actionPath, actionName)
			if(err) throw err

			// set window.state for easy debugging if available
			else if(typeof window == 'undefined') state = newState
			else window.state = state = newState

			// render if state is different
			if(old !== state && render) render(state)

			return state
		})
	})

	if(render) {
		render = render(runAction)
		// render initial state
		render(state)
	}

	// return action
	return runAction
}

// see afterUpdate on how this is used internally
export const dict = (o, empty = false) =>
	Object.defineProperty(empty? {} : o, dictSymbol, {value: Object.values(o)[0]})

export const list = (xs, empty = false) =>
	Object.defineProperty(empty? [] : xs, listSymbol, {value: xs[0]})

export const getter = f =>
	Object.defineProperty(f, getterSymbol, {value: true, writable: true})

// path can be a string or an array
// v can be a value or an function that takes the previous value and returns the new value
// returns a promise
export const set = curry((path, v, xs) =>
	update(resolvePath(path, xs), isF(v)? v(get(path, xs)) : v, xs))

export const get = curry((path, xs) =>
	followPath(xs)(path))

// a pipe function that works with promises
export const pipe = (...fns) => xs =>
	fns.reduce((a, c) => a.then(c), Promise.resolve(xs))

// v can only be a value and not a function
// returns a promise
export const append = curry((path, v, xs) =>
	set(path, x => x.concat(v), xs))

// remove key or index from object
// returns promise
export const remove = curry((path, xs) => {
	path = toArrPath(path)
	const [k, ...p] = path.reverse()
	const removeIndex = (k, xs) => [...xs.slice(0, +k), ...xs.slice(+k +1)]
	const removeKey = (k, {[k]: _, ...rest}) => rest
	return set(p, x => (Array.isArray(x)? removeIndex : removeKey)(k, x), xs)
})

// insert into list
// returns promise
export const insert = curry((path, v, xs) => {
	path = toArrPath(path)
	const [k, ...p] = path.reverse()
	return set(p, x => [...x.slice(0, +k), v, ...x.slice(+k)], xs)
})